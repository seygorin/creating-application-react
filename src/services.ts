import { UserRegistration } from './components/Registration/Registration.types';
import { User, Course, Author } from './store/store.types';

const BASE_URL = 'http://localhost:4000';

export const loginUser = async (email: string, password: string) => {
	try {
		const response = await fetch(`${BASE_URL}/login`, {
			method: 'POST',
			body: JSON.stringify({ email, password }),
			headers: {
				'Content-Type': 'application/json',
			},
		});

		const data: { result: string } = await response.json();
		return data.result;
	} catch (error) {
		console.error(error);
	}
};

export const registerUser = async (
	name: string,
	password: string,
	email: string
) => {
	const newUser: UserRegistration = {
		name,
		password,
		email,
	};

	try {
		const response = await fetch(`${BASE_URL}/register`, {
			method: 'POST',
			body: JSON.stringify(newUser),
			headers: {
				'Content-Type': 'application/json',
			},
		});

		const result: { message: string } = await response.json();
		return result;
	} catch (error) {
		console.error(error);
	}
};

export const fetchAuthors = async (): Promise<Author[] | undefined> => {
	try {
		const response = await fetch(`${BASE_URL}/authors/all`);
		const data: { result: Author[] } = await response.json();
		return data.result;
	} catch (error) {
		console.error(error);
	}
};

export const addAuthor = async (
	author: Author,
	token: string | null
): Promise<Author> => {
	const response = await fetch(`${BASE_URL}/authors/add`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
		body: JSON.stringify(author),
	});
	return response.json();
};

export const deleteAuthors = async (
	authorId: string,
	token: string | null
): Promise<{ message: string } | undefined> => {
	try {
		const response = await fetch(`${BASE_URL}/authors/${authorId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
		});
		const data: { message: string } = await response.json();
		return data;
	} catch (error) {
		console.error(error);
	}
};

export const fetchCourses = async (): Promise<Course[] | undefined> => {
	try {
		const response = await fetch(`${BASE_URL}/courses/all`);
		const data: { result: Course[] } = await response.json();
		return data.result;
	} catch (error) {
		console.error(error);
	}
};

export const addCourse = async (
	course: Course,
	token: string | null
): Promise<Course> => {
	const response = await fetch(`${BASE_URL}/courses/add`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
		body: JSON.stringify(course),
	});
	return response.json();
};

export const updateCourse = async (
	courseId: string,
	course: Course,
	token: string | null
): Promise<Course> => {
	const response = await fetch(`${BASE_URL}/courses/${courseId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `${token}`,
		},
		body: JSON.stringify(course),
	});
	const updatedCourse = await response.json();
	return updatedCourse;
};

export const deleteCourse = async (
	courseId: string,
	token: string | null
): Promise<{ message: string } | undefined> => {
	try {
		const response = await fetch(`${BASE_URL}/courses/${courseId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
		});
		const data: { message: string } = await response.json();
		return data;
	} catch (error) {
		console.error(error);
	}
};

export const fetchUser = async (token: string): Promise<User | undefined> => {
	try {
		const response = await fetch(`${BASE_URL}/users/me`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
		});
		const data: { result: User } = await response.json();
		const result = data.result;
		return result;
	} catch (error) {
		console.error(error);
	}
};

export const logoutUser = async (token: string): Promise<void> => {
	try {
		await fetch(`${BASE_URL}/logout`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
		});
	} catch (error) {
		console.error(error);
	}
};
