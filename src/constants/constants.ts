export const BUTTON_SEARCH = 'Search';
export const BUTTON_BACK = 'Back';
export const BUTTON_SHOW_COURSE = 'Show Course';
export const BUTTON_ADD_NEW_COURSE = 'Add new Course';
export const BUTTON_LOGIN = 'Login';
export const BUTTON_LOGOUT = 'Logout';
export const BUTTON_CANCEL = 'Cancel';
export const BUTTON_CREATE_COURSE = 'Create Course';
export const CREATE_AUTHOR = 'Create Author';
export const UPDATE_COURSE_BUTTON = 'Update Course';
