import React from 'react';

import { InputProps } from './Input.types';

import styles from './Input.module.css';

export const Input: React.FC<InputProps> = ({
	labelText,
	placeholderText,
	onChange,
	className,
	error,
	type,
	value,
	name,
}) => {
	const inputClass = `${styles.input} ${error ? styles.inputError : ''}`;

	return (
		<div className={`${styles.inputContainer} ${className}`}>
			<label className={styles.labelText}>{labelText}</label>
			<input
				type={type}
				value={value}
				name={name}
				placeholder={placeholderText}
				onChange={onChange}
				className={inputClass}
			/>
			{error && <div className={styles.error}>{error}</div>}
		</div>
	);
};
