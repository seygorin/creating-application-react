export type InputProps = {
	labelText: string;
	placeholderText: string;
	className?: string;
	onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
	error?: string;
	type?: 'text' | 'number' | 'password';
	value?: string | number;
	name?: string;
};
