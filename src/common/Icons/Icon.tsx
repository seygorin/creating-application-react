import React from 'react';

import { IconProps } from './Icon.types';

const Icon: React.FC<IconProps> = ({ src, alt, onClick }) => {
	return (
		<img src={src} alt={alt} onClick={onClick} style={{ cursor: 'pointer' }} />
	);
};

export default Icon;
