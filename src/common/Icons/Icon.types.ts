export type IconProps = {
	src: string;
	alt?: string;
	onClick?: () => void;
};
