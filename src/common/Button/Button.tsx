import React from 'react';

import styles from './Button.module.css';
import { ButtonProps } from './Button.types';

export const Button: React.FC<ButtonProps> = ({
	buttonText,
	onClick,
	className,
	type,
	form,
}) => {
	return (
		<button
			form={form}
			type={type}
			className={`${styles.button} ${className}`}
			onClick={onClick}
		>
			{buttonText}
		</button>
	);
};
