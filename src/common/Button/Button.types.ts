export type ButtonProps = {
	buttonText: string | React.ReactNode;
	onClick?: () => void;
	className?: string;
	type?: 'button' | 'submit' | 'reset';
	form?: string;
};
