export type TextareaProps = {
	value: string;
	onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
	placeholder?: string;
	labelText?: string;
	className?: string;
	error?: string;
	rows?: number;
	cols?: number;
	name?: string;
};
