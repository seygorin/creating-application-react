import React from 'react';

import { TextareaProps } from './Textarea.types';
import styles from './Textarea.module.css';

export const Textarea: React.FC<TextareaProps> = ({
	labelText,
	onChange,
	placeholder,
	className,
	error,
	value,
	name,
	rows = 2,
	cols = 30,
}) => {
	const textareaClass = `${styles.textarea} ${error ? styles.textareaError : ''}`;

	return (
		<div className={`${styles.textareaContainer} ${className}`}>
			<label className={styles.labelText}>{labelText}</label>
			<textarea
				rows={rows}
				cols={cols}
				value={value}
				className={textareaClass}
				onChange={onChange}
				placeholder={placeholder}
				name={name}
			/>

			{error && <div className={styles.error}>{error}</div>}
		</div>
	);
};
