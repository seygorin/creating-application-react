import { createAsyncThunk } from '@reduxjs/toolkit';
import {
	fetchAuthors as fetchAuthorsService,
	addAuthor as addAuthorService,
	deleteAuthors as deleteAuthorService,
} from '../../services';

import { Author } from '../store.types';

export const fetchAuthors = createAsyncThunk('authors/fetch', async () => {
	const authors = await fetchAuthorsService();
	return authors;
});

const token = localStorage.getItem('token');

export const addAuthors = createAsyncThunk(
	'authors/add',
	async (author: Author) => {
		const response = await addAuthorService(author, token);
		return response.result;
	}
);

export const deleteAuthors = createAsyncThunk(
	'authors/delete',
	async (id: string) => {
		await deleteAuthorService(id, token);
		return id;
	}
);
