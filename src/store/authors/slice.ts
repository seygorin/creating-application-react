import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchAuthors, addAuthors, deleteAuthors } from './thunk';
import { Author } from '../store.types';

const initialState: Author[] = [];

const authorsSlice = createSlice({
	name: 'authors',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(
			fetchAuthors.fulfilled,
			(state, action: PayloadAction<Author[] | undefined>) => {
				return action.payload;
			}
		);
		builder.addCase(
			addAuthors.fulfilled,
			(state, action: PayloadAction<Author>) => {
				state.push(action.payload);
			}
		);
		builder.addCase(
			deleteAuthors.fulfilled,
			(state, action: PayloadAction<string>) => {
				return state.filter((authors) => authors.id !== action.payload);
			}
		);
	},
});

export default authorsSlice.reducer;
