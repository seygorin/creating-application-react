import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchCourses, addCourse, deleteCourse, updateCourse } from './thunk';
import { Course } from '../store.types';

const initialState: Course[] = [];

const coursesSlice = createSlice({
	name: 'courses',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(
			fetchCourses.fulfilled,
			(state, action: PayloadAction<Course[] | undefined>) => {
				return action.payload;
			}
		);
		builder.addCase(
			addCourse.fulfilled,
			(state, action: PayloadAction<Course>) => {
				state.push(action.payload);
			}
		);
		builder.addCase(
			updateCourse.fulfilled,
			(
				state,
				action: PayloadAction<{ successful: boolean; result: Course }>
			) => {
				const updatedCourse = action.payload.result;
				const index = state.findIndex((c) => c.id === updatedCourse.id);
				if (index !== -1) {
					state[index] = updatedCourse;
				}
			}
		);
		builder.addCase(
			deleteCourse.fulfilled,
			(state, action: PayloadAction<string>) => {
				return state.filter((course) => course.id !== action.payload);
			}
		);
	},
});

export default coursesSlice.reducer;
