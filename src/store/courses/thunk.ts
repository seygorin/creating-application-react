import { createAsyncThunk } from '@reduxjs/toolkit';
import {
	fetchCourses as fetchCoursesService,
	addCourse as addCourseService,
	updateCourse as updateCourseService,
	deleteCourse as deleteCourseService,
} from '../../services';

import { Course } from '../store.types';

export const fetchCourses = createAsyncThunk('courses/fetch', async () => {
	const courses = await fetchCoursesService();
	return courses;
});

const token = localStorage.getItem('token');

export const addCourse = createAsyncThunk(
	'courses/add',
	async (course: Course) => {
		const response = await addCourseService(course, token);
		return response.result;
	}
);

export const updateCourse = createAsyncThunk(
	'courses/update',
	async ({ courseId, course }: { courseId: string; course: Course }) => {
		const response = await updateCourseService(courseId, course, token);
		return response;
	}
);

export const deleteCourse = createAsyncThunk(
	'courses/delete',
	async (id: string) => {
		await deleteCourseService(id, token);
		return id;
	}
);
