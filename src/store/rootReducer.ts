import { combineReducers } from '@reduxjs/toolkit';
import coursesSlice from './courses/slice';
import authorsSlice from './authors/slice';
import userSlice from './user/slice';

import { Store } from './store.types';

const rootReducer = combineReducers({
	courses: coursesSlice,
	authors: authorsSlice,
	user: userSlice,
});

export type RootState = Store;

export default rootReducer;
