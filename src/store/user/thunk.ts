import { createAsyncThunk } from '@reduxjs/toolkit';
import {
	fetchUser as fetchUserService,
	logoutUser as logoutUserService,
} from '../../services';

export const fetchUsers = createAsyncThunk(
	'users/fetch',
	async (token: string) => {
		const user = await fetchUserService(token);
		return user;
	}
);

export const logoutUser = createAsyncThunk(
	'user/logout',
	async (token: string) => {
		await logoutUserService(token);
	}
);
