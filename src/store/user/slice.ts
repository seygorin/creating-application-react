import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchUsers, logoutUser } from './thunk';
import { User } from '../store.types';

export const initialState: User = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(
				fetchUsers.fulfilled,
				(state, action: PayloadAction<User | undefined>) => {
					return action.payload;
				}
			)
			.addCase(logoutUser.fulfilled, () => {
				return initialState;
			});
	},
});

export default userSlice.reducer;
