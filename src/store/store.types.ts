export type Course = {
	id?: string;
	title: string;
	description: string;
	creationDate?: string;
	duration: number;
	authors: string[];
	result?: string;
};

export type Author = {
	id: string;
	name: string;
	result?: string;
};

export type User = {
	isAuth: boolean;
	name: string;
	email: string;
	token: string;
	role: string;
};

export type Store = {
	user: User;
	courses: Course[];
	authors: Author[];
};
