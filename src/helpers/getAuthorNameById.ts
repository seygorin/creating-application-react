import { Author } from '../components/Courses/Courses.types';

const getAuthorNameById = (id: string, authors: Author[]) => {
	const author = authors.find((author) => author.id === id);
	return author ? author.name : '';
};

export default getAuthorNameById;
