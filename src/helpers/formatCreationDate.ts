const formatCreationDate = (date: string) => {
	const [day, month, year] = date.split('/');
	const d = new Date(`${month}/${day}/${year}`);
	return `${String(d.getDate()).padStart(2, '0')}.${String(d.getMonth() + 1).padStart(2, '0')}.${d.getFullYear()}`;
};

export default formatCreationDate;
