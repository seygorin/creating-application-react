import React from 'react';
import { useParams, Link } from 'react-router-dom';

import { Button } from '../../common/Button/Button';
import { BUTTON_BACK } from '../../constants/constants';

import styles from './CourseInfo.module.css';

import formatCreationDate from '../../helpers/formatCreationDate';
import getCourseDuration from '../../helpers/getCourseDuration';
import getAuthorNameById from '../../helpers/getAuthorNameById';

import { CourseInfoProps } from '../Courses/Courses.types';

const CourseInfo: React.FC<CourseInfoProps> = ({ courses, authors }) => {
	const getAuthorName = (id: string) => getAuthorNameById(id, authors);
	const { courseId } = useParams();
	const selectedCourse = courses.find((c) => c.id === courseId);

	if (!selectedCourse) {
		return <div className={styles.notFound}>Course not found</div>;
	}

	return (
		<div className={styles.courseInfo}>
			<h1 className={styles.courseTitle}>{selectedCourse.title}</h1>
			<div className={styles.courseCard}>
				<h2>Description:</h2>
				<div className={styles.cardBody}>
					<div className={styles.courseDetails}>
						<p>{selectedCourse.description}</p>
					</div>
					<div className={styles.courseActions}>
						<p>
							<strong>ID: </strong> {selectedCourse.id}
						</p>
						<p className={styles.cardTextEllipsis}>
							<strong>Duration: </strong>
							{getCourseDuration(selectedCourse.duration)}
						</p>
						<p>
							<strong>Created: </strong>
							{formatCreationDate(selectedCourse.creationDate)}
						</p>
						<p className={styles.cardTextEllipsis}>
							<strong>Authors: </strong>
							{selectedCourse.authors.map(getAuthorName).join(', ')}
						</p>
					</div>
				</div>
			</div>

			<Link to='/courses'>
				<Button buttonText={BUTTON_BACK} className={styles.cardButton} />
			</Link>
		</div>
	);
};

export default CourseInfo;
