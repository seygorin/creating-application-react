import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

import { getUser } from '../../store/selectors';

const PrivateRoute = ({ children }) => {
	const user = useSelector(getUser);

	return user.role === 'admin' ? children : <Navigate to='/courses' />;
};

export default PrivateRoute;
