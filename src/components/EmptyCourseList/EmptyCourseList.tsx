import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getUser } from '../../store/selectors';
import { Button } from '../../common/Button/Button';
import { BUTTON_ADD_NEW_COURSE } from '../../constants/constants';

import styles from './EmptyCourseList.module.css';

const EmptyCourseList: React.FC = () => {
	const user = useSelector(getUser);

	return (
		<div className={styles.emptyContainer}>
			<h2>Your List Is Empty</h2>
			{user.role === 'admin' ? (
				<Link to='/courses/add'>
					<Button
						buttonText={BUTTON_ADD_NEW_COURSE}
						className={styles.emptyButton}
					/>
				</Link>
			) : (
				<p className={styles.emptyText}>
					You don't have permissions to create a course. Please log in as ADMIN
				</p>
			)}
		</div>
	);
};

export default EmptyCourseList;
