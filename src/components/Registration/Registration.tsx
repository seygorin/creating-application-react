import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { Input } from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';
import { BUTTON_LOGIN } from '../../constants/constants';

import { registerUser } from '../../services';

import styles from './Registration.module.css';

const Registration: React.FC = () => {
	const [values, setValues] = useState<{ [key: string]: string }>({
		name: '',
		email: '',
		password: '',
	});
	const [errors, setErrors] = useState<{ [key: string]: string }>({
		name: '',
		email: '',
		password: '',
	});

	const navigate = useNavigate();

	const validateForm = () => {
		let isValid = true;
		let newErrors = { ...errors };

		if (values.password.length < 6) {
			newErrors = {
				...newErrors,
				password: 'Password must be at least 6 characters',
			};
			isValid = false;
		}

		if (!values.email.endsWith('email.com')) {
			newErrors = {
				...newErrors,
				email: 'Email must end with email.com',
			};
			isValid = false;
		}

		if (values.name.length < 2) {
			newErrors = {
				...newErrors,
				name: 'Name must be at least 2 characters',
			};
			isValid = false;
		}

		setErrors(newErrors);
		return isValid;
	};
	const handleSubmit = async (event: React.FormEvent) => {
		event.preventDefault();

		if (validateForm()) {
			await registerUser(values.name, values.password, values.email);
			navigate('/login');
		}
	};
	const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = event.target;

		setValues({
			...values,
			[name]: value,
		});

		if (errors[name]) {
			setErrors({
				...errors,
				[name]: '',
			});
		}
	};
	return (
		<div className={styles.registration}>
			<h2 className={styles.registrationTitle}>Registration</h2>
			<div className={styles.formContainer}>
				<form onSubmit={handleSubmit} className={styles.registrationForm}>
					<Input
						labelText='Name'
						placeholderText='Input text'
						value={values.name}
						name='name'
						onChange={handleInputChange}
						className={styles.inputForm}
						error={errors.name}
					/>
					<Input
						labelText='Email'
						placeholderText='Input text'
						name='email'
						value={values.email}
						onChange={handleInputChange}
						className={styles.inputForm}
						error={errors.email}
					/>
					<Input
						labelText='Password'
						placeholderText='Input text'
						value={values.password}
						name='password'
						type='password'
						onChange={handleInputChange}
						className={styles.inputForm}
						error={errors.password}
					/>
					<Button
						className={styles.buttonForm}
						buttonText={BUTTON_LOGIN}
						onClick={() => {}}
					/>

					<div className={styles.link}>
						If you have an account you may
						<Link to='/login'>
							<strong> Login</strong>
						</Link>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Registration;
