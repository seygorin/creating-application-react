export type AuthorItemProps = {
	name: string;
	onAdd?: () => void;
	onDelete?: () => void;
};
