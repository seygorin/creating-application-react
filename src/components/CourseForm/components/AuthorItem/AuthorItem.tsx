import React from 'react';

import addIcon from '../../../../assets/add.svg';
import deleteIcon from '../../../../assets/trash.svg';
import Icons from '../../../../common/Icons/Icon';

import { AuthorItemProps } from './AuthorItem.types';

import styles from './AuthorItem.module.css';

const AuthorItem: React.FC<AuthorItemProps> = ({ name, onAdd, onDelete }) => {
	return (
		<div className={styles.authorItem}>
			<span className={styles.name}>{name}</span>
			<div className={styles.icons}>
				{onAdd && <Icons src={addIcon} alt={'add author'} onClick={onAdd} />}
				{onDelete && (
					<Icons src={deleteIcon} alt={'delete author'} onClick={onDelete} />
				)}
			</div>
		</div>
	);
};

export default AuthorItem;
