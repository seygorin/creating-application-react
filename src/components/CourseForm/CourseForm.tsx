import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';

import { AppDispatch } from '../../store';
import { RootState } from '../../store/rootReducer';
import { addCourse, updateCourse } from '../../store/courses/thunk';
import { addAuthors, deleteAuthors } from '../../store/authors/thunk';

import { Input } from '../../common/Input/Input';
import { Textarea } from '../../common/Textarea/Textarea';
import { Button } from '../../common/Button/Button';
import AuthorItem from './components/AuthorItem/AuthorItem';

import styles from './CourseForm.module.css';

import {
	BUTTON_CREATE_COURSE,
	BUTTON_CANCEL,
	CREATE_AUTHOR,
	UPDATE_COURSE_BUTTON,
} from '../../constants/constants';

import { Author, CourseFormProps, FormState } from '../Courses/Courses.types';

const CourseForm: React.FC<CourseFormProps> = ({ authors }) => {
	const navigate = useNavigate();
	const dispatch: AppDispatch = useDispatch();

	const courseId = useParams().courseId;
	const course = useSelector((state: RootState) =>
		state.courses.find((course) => course.id === courseId)
	);

	useEffect(() => {
		if (course) {
			setValues({
				title: course.title,
				description: course.description,
				duration: course.duration,
			});
			const courseAuthors = authors.filter((author) =>
				course.authors.includes(author.id)
			);
			setCourseAuthors(courseAuthors);

			const remainingAuthors = authors.filter(
				(author) => !course.authors.includes(author.id)
			);
			setAuthor(remainingAuthors);
		}
	}, [course]);
	const [values, setValues] = useState<FormState>({
		title: '',
		description: '',
		authorName: '',
		duration: 0,
	});
	const [errors, setErrors] = useState<{ [key: string]: string }>({
		title: '',
		description: '',
		duration: '',
	});
	const [author, setAuthor] = useState<Author[]>(authors);
	const [courseAuthors, setCourseAuthors] = useState<Author[]>([]);
	const [error, setError] = useState<string | null>(null);

	const validateForm = () => {
		let titleError = '';
		let descriptionError = '';
		let durationError = '';

		if (values.title.length < 2) titleError = 'Title is required.';
		if (values.description.length < 2)
			descriptionError = 'Description is required.';
		if (values.duration <= 0) durationError = 'Duration is required.';

		if (titleError || descriptionError || durationError) {
			setErrors({
				title: titleError,
				description: descriptionError,
				duration: durationError,
			});
			return false;
		} else {
			setErrors({
				title: '',
				description: '',
				duration: '',
			});
		}

		return true;
	};

	const handleInputChange = (
		event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
	) => {
		const { name, value } = event.target;

		setValues({
			...values,
			[name]: name === 'duration' ? Number(value) : value,
		});

		if (errors[name]) {
			setErrors({
				...errors,
				[name]: '',
			});
		}
	};
	const handleCreateOrUpdateCourse = async (event: React.FormEvent) => {
		event.preventDefault();
		if (validateForm()) {
			const courseData = {
				title: values.title,
				description: values.description,
				duration: values.duration,
				authors: courseAuthors.map((author) => author.id),
			};
			try {
				let resultAction;
				if (courseId) {
					resultAction = await dispatch(
						updateCourse({ courseId, course: courseData })
					);
					navigate(`/courses/${courseId}`);
				} else {
					resultAction = await dispatch(addCourse(courseData));
					const course = unwrapResult(resultAction);
					navigate(`/courses/${course.id}`);
				}
			} catch (err) {
				console.log('Failed to create or update course: ', err);
			}
		} else {
			console.log('Form is not valid');
		}
	};

	const handleAddAuthor = (author: Author) => {
		setCourseAuthors((prevCourseAuthors) => [...prevCourseAuthors, author]);
		setAuthor((prevAuthors) => prevAuthors.filter((a) => a.id !== author.id));
	};

	const handleDeleteAuthor = (author: Author) => {
		setCourseAuthors((prevCourseAuthors) =>
			prevCourseAuthors.filter((a) => a.id !== author.id)
		);

		setAuthor((prevAuthors) => [...prevAuthors, author]);
	};

	const handleDeleteFromAuthors = (author: Author) => {
		dispatch(deleteAuthors(author.id));
		setAuthor((prevAuthors) => prevAuthors.filter((a) => a.id !== author.id));
	};

	const handleCreateAuthor = () => {
		if (!values.authorName || values.authorName.trim() === '') {
			setError('Author name cannot be empty');
			return;
		}

		const newAuthor: Author = {
			id: String(Date.now()),
			name: values.authorName,
		};

		dispatch(addAuthors(newAuthor));

		setAuthor((prevAuthors) => [...prevAuthors, newAuthor]);
		setValues({
			...values,
			authorName: '',
		});
		setError(null);
	};

	const formatDuration = (minutes: number) => {
		const hours = Math.floor(minutes / 60);
		const mins = minutes % 60;
		return `${hours.toString().padStart(2, '0')}:${mins.toString().padStart(2, '0')}`;
	};

	return (
		<div className={styles.createCourse}>
			<h2 className={styles.createCourseTitle}>Course Edit/Create Page</h2>
			<div className={styles.createCourseContainer}>
				<form
					id='formCreateCourse'
					onSubmit={handleCreateOrUpdateCourse}
					className={styles.createCourseForm}
				>
					<div>
						<h3>Main Info</h3>
						<Input
							labelText='Title'
							placeholderText='Input Text'
							name='title'
							value={values.title}
							onChange={handleInputChange}
							className={styles.inputForm}
							error={errors.title}
						/>

						<Textarea
							labelText='Description'
							placeholder='Input Text'
							name='description'
							onChange={handleInputChange}
							value={values.description}
							className={styles.inputForm}
							error={errors.description}
							rows={8}
						/>
					</div>

					<div>
						<h3>Duration</h3>
						<div className={styles.authors}>
							<Input
								labelText='Duration'
								name='duration'
								placeholderText='Input Text'
								value={values.duration}
								onChange={handleInputChange}
								className={`${styles.inputForm} ${styles.inputFormSmall}`}
								error={errors.duration}
								type='number'
							/>
							<p>
								<strong>{formatDuration(values.duration)}</strong> hours
							</p>
						</div>
					</div>

					<div className={styles.auhtorsContainer}>
						<div>
							<h3>Authors</h3>
							<div className={styles.authors}>
								<Input
									labelText='Author Name'
									placeholderText='Input Text'
									value={values.authorName}
									name='authorName'
									onChange={handleInputChange}
									className={`${styles.inputForm} ${styles.inputFormSmall}`}
								/>

								<Button
									type={'button'}
									className={styles.buttonAuthors}
									buttonText={CREATE_AUTHOR}
									onClick={handleCreateAuthor}
								/>
							</div>
							{error && <div className={styles.errorAuthor}>{error}</div>}
						</div>
						<div>
							<h3>Course Authors</h3>
							<div className={styles.courseAuthors}>
								{courseAuthors.length === 0 ? (
									<p>Author list is empty</p>
								) : (
									courseAuthors.map((author) => (
										<AuthorItem
											key={author.id}
											name={author.name}
											onDelete={() => handleDeleteAuthor(author)}
										/>
									))
								)}
							</div>
						</div>
					</div>

					<div className={styles.authorsList}>
						<h4>Authors List</h4>
						{author.length === 0 ? (
							<p>Author list is empty</p>
						) : (
							author.map((author) => (
								<AuthorItem
									key={author.id}
									name={author.name}
									onAdd={() => handleAddAuthor(author)}
									onDelete={() => handleDeleteFromAuthors(author)}
								/>
							))
						)}
					</div>
				</form>
			</div>

			<div className={styles.buttonsCreate}>
				<Link to='/courses'>
					<Button
						className={styles.buttonFormCreate}
						buttonText={BUTTON_CANCEL}
						onClick={() => {}}
					/>
				</Link>

				<Button
					form='formCreateCourse'
					type={'submit'}
					className={`${styles.buttonFormCreate} ${styles.lastButton}`}
					buttonText={courseId ? UPDATE_COURSE_BUTTON : BUTTON_CREATE_COURSE}
					onClick={() => handleCreateOrUpdateCourse}
				/>
			</div>
		</div>
	);
};

export default CourseForm;
