import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import styles from './Main.module.css';

const Main: React.FC = () => {
	const navigate = useNavigate();

	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token) {
			navigate('/courses');
		}
	}, []);

	return (
		<div className={styles.main}>
			<h1>Main page</h1>
			<p>
				Try to <Link to='/login'>Login</Link>
			</p>
		</div>
	);
};

export default Main;
