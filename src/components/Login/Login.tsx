import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { AppDispatch } from '../../store';
import { fetchUsers } from '../../store/user/thunk';

import { Input } from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';
import { BUTTON_LOGIN } from '../../constants/constants';

import { loginUser } from '../../services';

import styles from './Login.module.css';

const Login: React.FC = () => {
	const dispatch: AppDispatch = useDispatch();

	const [values, setValues] = useState<{ [key: string]: string }>({
		email: '',
		password: '',
	});
	const [errors, setErrors] = useState<{ [key: string]: string }>({
		email: '',
		password: '',
	});

	const navigate = useNavigate();

	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token) {
			navigate('/courses');
		}
	}, []);

	const validateForm = () => {
		let isValid = true;
		let newErrors = { ...errors };

		if (values.password.length < 6) {
			newErrors = {
				...newErrors,
				password: 'Password must be at least 6 characters',
			};
			isValid = false;
		}

		if (!values.email.endsWith('email.com')) {
			newErrors = {
				...newErrors,
				email: 'Email must end with email.com',
			};
			isValid = false;
		}

		setErrors(newErrors);
		return isValid;
	};

	const handleSubmit = async (event: React.FormEvent) => {
		event.preventDefault();

		if (validateForm()) {
			const token = await loginUser(values.email, values.password);
			if (token) {
				await localStorage.setItem('token', token);
				dispatch(fetchUsers(token));
			}
			navigate('/courses');
		}
	};

	const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = event.target;

		setValues({
			...values,
			[name]: value,
		});

		if (errors[name]) {
			setErrors({
				...errors,
				[name]: '',
			});
		}
	};
	return (
		<div className={styles.login}>
			<h2 className={styles.loginTitle}>Login</h2>
			<div className={styles.loginContainer}>
				<form onSubmit={handleSubmit} className={styles.loginForm}>
					<Input
						labelText='Email'
						placeholderText='Input text'
						name='email'
						onChange={handleInputChange}
						className={styles.inputForm}
						error={errors.email}
					/>

					<Input
						labelText='Password'
						placeholderText='Input text'
						name='password'
						type='password'
						onChange={handleInputChange}
						className={styles.inputForm}
						error={errors.password}
					/>

					<Button
						className={styles.buttonForm}
						buttonText={BUTTON_LOGIN}
						onClick={() => {}}
					/>

					<div className={styles.registration}>
						If you don't have an account you may
						<Link to='/registration'>
							<strong> Registration</strong>
						</Link>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Login;
