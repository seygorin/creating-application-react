import React from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { AppDispatch } from '../../store';
import { getUser } from '../../store/selectors';
import { logoutUser } from '../../store/user/thunk';

import { Button } from '../../common/Button/Button';
import { BUTTON_LOGIN, BUTTON_LOGOUT } from '../../constants/constants';
import { Logo } from './components/Logo/Logo';

import styles from './Header.module.css';

const Header: React.FC = () => {
	const user = useSelector(getUser);

	const dispatch: AppDispatch = useDispatch();

	const navigate = useNavigate();
	const location = useLocation();
	const isAuthPage =
		location.pathname === '/login' || location.pathname === '/registration';

	const handleLogout = () => {
		if (!user) {
			return;
		}
		const token = localStorage.getItem('token');
		if (!token) {
			return;
		}

		localStorage.removeItem('token');
		dispatch(logoutUser(token));
		navigate('/login');
	};

	return (
		<header className={styles.header}>
			<Link to='/courses'>
				<Logo />
			</Link>
			{!isAuthPage && (
				<div className={styles.loginContainer}>
					<span className={styles.userName}>
						{user && user.name !== null ? user.name : 'admin'}
					</span>
					<Link to='/login'>
						<Button
							buttonText={user.isAuth === false ? BUTTON_LOGIN : BUTTON_LOGOUT}
							onClick={handleLogout}
							className={styles.loginButton}
						/>
					</Link>
				</div>
			)}
		</header>
	);
};

export default Header;
