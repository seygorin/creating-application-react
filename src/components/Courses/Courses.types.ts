export type Course = {
	id?: string;
	title: string;
	creationDate?: string;
	description: string;
	duration: number;
	authors: string[];
};

export type Author = {
	id: string;
	name: string;
};

export type CourseProps = {
	courses: Course[];
	authors: Author[];
};

export type CourseInfoProps = {
	courses: Course[];
	authors: Author[];
};

export type CourseCardProps = {
	course: Course;
	authors: Author[];
};

export type CourseFormProps = {
	authors: Author[];
};

export type FormState = {
	title: string;
	description: string;
	duration: number;
	authorName?: string;
};

export type SearchBarProps = {
	courses: Course[];
};
