import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { Button } from '../../common/Button/Button';
import { BUTTON_ADD_NEW_COURSE } from '../../constants/constants';

import CourseCard from './components/CourseCard/CourseCard';
import EmptyCourseList from '../EmptyCourseList/EmptyCourseList';
import CourseInfo from '../CourseInfo/CourseInfo';
import SearchBar from './components/SearchBar/SearchBar';
import { getUser } from '../../store/selectors';

import styles from './Courses.module.css';

import { CourseProps } from './Courses.types';

const Courses: React.FC<CourseProps> = ({ courses, authors }) => {
	const userRole = useSelector(getUser);

	if (!courses) {
		return <CourseInfo courses={courses} authors={authors} />;
	}

	return (
		<div className={styles.course}>
			{courses.length === 0 ? (
				<EmptyCourseList />
			) : (
				<>
					<div className={styles.searchContainer}>
						<SearchBar courses={courses} />
						{userRole.role === 'admin' && (
							<Link to='/courses/add'>
								<Button
									buttonText={BUTTON_ADD_NEW_COURSE}
									className={styles.addCourseButton}
								/>
							</Link>
						)}
					</div>

					{courses.map((course) => (
						<CourseCard key={course.id} course={course} authors={authors} />
					))}
				</>
			)}
		</div>
	);
};

export default Courses;
