import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { AppDispatch } from '../../../../store';
import { deleteCourse } from '../../../../store/courses/thunk';
import { Button } from '../../../../common/Button/Button';
import { BUTTON_SHOW_COURSE } from '../../../../constants/constants';

import styles from './CourseCard.module.css';

import formatCreationDate from '../../../../helpers/formatCreationDate';
import getCourseDuration from '../../../../helpers/getCourseDuration';
import getAuthorNameById from '../../../../helpers/getAuthorNameById';
import { getUser } from '../../../../store/selectors';

import trashIcon from '../../../../assets/course-trash.svg';
import editIcon from '../../../../assets/course-edit.svg';
import Icons from '../../../../common/Icons/Icon';

import { CourseCardProps } from '../../Courses.types';

const CourseCard: React.FC<CourseCardProps> = ({ course, authors }) => {
	const dispatch: AppDispatch = useDispatch();

	const {
		title,
		description,
		creationDate,
		duration,
		authors: authorIds,
	} = course;
	const userRole = useSelector(getUser);

	const getAuthorName = (id: string) => getAuthorNameById(id, authors);

	const handleDelete = () => {
		dispatch(deleteCourse(course.id));
	};

	return (
		<div className={styles.card}>
			<h2>{title}</h2>
			<div className={styles.cardBody}>
				<div className={styles.courseDetails}>
					<p className={styles.cardText}>{description}</p>
				</div>
				<div className={styles.courseActions}>
					<p className={`${styles.cardText} ${styles.cardTextEllipsis}`}>
						<strong>Authors: </strong>
						{authorIds.map(getAuthorName).join(', ')}
					</p>
					<p className={`${styles.cardText} ${styles.cardTextEllipsis}`}>
						<strong>Duration: </strong> {getCourseDuration(duration)}
					</p>
					<p className={styles.cardText}>
						<strong>Created: </strong> {formatCreationDate(creationDate)}
					</p>
					<div className={styles.cardButtonCoitainer}>
						<Link to={`/courses/${course.id}`}>
							<Button
								buttonText={BUTTON_SHOW_COURSE}
								className={styles.cardButton}
							/>
						</Link>
						{userRole.role === 'admin' && (
							<div className={styles.cardButtonSmallContainer}>
								<Button
									buttonText={<Icons src={trashIcon} alt={'delete course'} />}
									onClick={handleDelete}
									className={styles.cardButtonSmall}
								/>
								<Link to={`/courses/update/${course.id}`}>
									<Button
										buttonText={<Icons src={editIcon} alt={'edit course'} />}
										className={styles.cardButtonSmall}
									/>
								</Link>
							</div>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
