import React, { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

import { Button } from '../../../../common/Button/Button';
import { BUTTON_SEARCH } from '../../../../constants/constants';
import { SearchBarProps } from '../../Courses.types';

import styles from './SearchBar.module.css';

const SearchBar: React.FC<SearchBarProps> = ({ courses, onFilterCourses }) => {
	const [searchTerm, setSearchTerm] = useState('');
	const navigate = useNavigate();
	const location = useLocation();

	const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSearchTerm(event.target.value);
	};

	const handleSearchSubmit = (event: React.FormEvent) => {
		event.preventDefault();
		if (searchTerm) {
			const lowercasedSearchTerm = searchTerm.toLowerCase();
			const filtered = courses.filter(
				(course) =>
					course.title.toLowerCase().includes(lowercasedSearchTerm) ||
					course.id.toLowerCase().includes(lowercasedSearchTerm)
			);
			onFilterCourses(filtered);
			navigate(`${location.pathname}?search=${searchTerm}`);
		} else {
			onFilterCourses(courses);
			navigate(location.pathname);
		}
	};
	const handleSearchReset = () => {
		onFilterCourses(courses);
		navigate(location.pathname);
	};

	return (
		<div>
			<form onSubmit={handleSearchSubmit} className={styles.searchBar}>
				<input
					type='text'
					value={searchTerm}
					onChange={handleSearchChange}
					onInput={handleSearchReset}
					placeholder='Input text'
					className={styles.searchInput}
				/>
				<Button
					className={styles.searchButton}
					type='submit'
					buttonText={BUTTON_SEARCH}
				/>
			</form>
		</div>
	);
};

export default SearchBar;
