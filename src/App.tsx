import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Routes, Route, BrowserRouter } from 'react-router-dom';

import { AppDispatch } from './store';
import { fetchCourses } from './store/courses/thunk';
import { fetchAuthors } from './store/authors/thunk';
import { fetchUsers } from './store/user/thunk';

import { getCourses, getAuthors } from './store/selectors';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';

import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseForm from './components/CourseForm/CourseForm';
import CourseInfo from './components/CourseInfo/CourseInfo';
import Main from './components/Main/Main';
import NotFound from './components/404/NotFound';

import PrivateRoute from './components/PrivateRoute/PrivateRoute';

import styles from './App.module.css';

const App: React.FC = () => {
	const dispatch: AppDispatch = useDispatch();
	const courses = useSelector(getCourses);
	const authors = useSelector(getAuthors);

	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token) {
			dispatch(fetchUsers(token));
		}
		dispatch(fetchCourses());
		dispatch(fetchAuthors());
	}, []);

	return (
		<BrowserRouter>
			<Header />
			<main className={styles.wrapper}>
				<Routes>
					<Route path='/' element={<Main />} />
					<Route
						path='/courses'
						element={<Courses courses={courses} authors={authors} />}
					/>
					<Route path='/registration' element={<Registration />} />
					<Route path='/login' element={<Login />} />
					<Route
						path='/courses/:courseId'
						element={<CourseInfo courses={courses} authors={authors} />}
					/>
					<Route
						path='/courses/add'
						element={
							<PrivateRoute>
								<CourseForm authors={authors} />
							</PrivateRoute>
						}
					/>
					<Route
						path='/courses/update/:courseId'
						element={
							<PrivateRoute>
								<CourseForm authors={authors} />
							</PrivateRoute>
						}
					/>
					<Route path='*' element={<NotFound />} />
				</Routes>
			</main>
		</BrowserRouter>
	);
};

export default App;
